package nightbot

import "fmt"

func (client *NightbotClient) SendChannelMessage(message string) error {
	var e NightbotError

	payload := map[string]string{
		"message": message,
	}

	resp, err := client.session.Post("https://api.nightbot.tv/1/channel/send", payload, nil, &e)
	if err != nil {
		return err
	}

	if resp.Status() != 200 {
		return fmt.Errorf("Status: %d, Message: %s", resp.Status(), e.Message)
	}

	return nil
}
