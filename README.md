# Nightbot API Client for Go

[![Build Status](https://drone.io/bitbucket.org/muxy/nightbot/status.png)](https://drone.io/bitbucket.org/muxy/nightbot/latest)
[![MIT](http://img.shields.io/badge/license-MIT-green.svg)](LICENSE)