package nightbot

import (
	"net/http"

	"gopkg.in/jmcvetta/napping.v3"
)

type NightbotClient struct {
	session napping.Session
}

func NewNightbotClient(httpClient *http.Client) NightbotClient {
	return NightbotClient{
		session: napping.Session{
			Client: httpClient,
		},
	}
}

type NightbotError struct {
	Status  int64  `json:"status"`
	Message string `json:"message"`
}
